using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MVC.Models;

namespace MVC.Controllers {

    public class ToolMan {
        public int ID { get; set; }

        public string Name { get; set; }

        public string DoingThing { get; set; }

        public DateTime DoingDatetime { get; set; }

    }
    public class BusinessController : Controller {
        public IActionResult YoMan () {
            return View ();

        }

        public IActionResult ToolMan () {

            var a = new ToolMan { ID = 1, Name = "RichWang" , DoingThing = "Buy Breakfast" , DoingDatetime = DateTime.Now};
            return Json (a);
        }

    }
}