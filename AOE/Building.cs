using System;
namespace AOE {

    public class Buliding {

        public string ID { get; set; }
        public BuildingType Category { get; set; }

        public string Produce (string Type) {
            string Type_Farmhouse = $"Farmhouse";
            string Type_Church = $"Church";
            

            if (Type == Type_Farmhouse) {
                return $"Farmhouse can produce {Occupation.Farmer}";
            } else if (Type == Type_Church) {
                return $"Chruch can produce {Occupation.Monk}";
            } else {
                return $"TrainCenter can produce {Occupation.Kinght}";
            }

        }

    }
    public enum BuildingType {
        Farmhouse,
        Church,
        TrainingCenter
    }
}