using System;
using System.Collections.Generic;
using AOE;

namespace AOE {
    class Military {
        static void Main (string[] args) {
            string Output = "";

            var AG = new ArmyGroup { };

            var Farmer = new Human { ID = "a", Job = Occupation.Farmer };
            var Monk = new Human { ID = "b", Job = Occupation.Monk };
            var Kinght = new Human { ID = "c", Job = Occupation.Kinght };

            var Farmhouse = new Buliding { ID = "x", Category = BuildingType.Farmhouse };
            var Church = new Buliding { ID = "y", Category = BuildingType.Church };
            var TrainingCenter = new Buliding { ID = "z", Category = BuildingType.TrainingCenter };

            AG.HumanList.Add (Farmer);
            AG.HumanList.Add (Kinght);
            AG.HumanList.Add (Monk);
            AG.BuildingList.Add (Farmhouse);
            AG.BuildingList.Add (Church);
            AG.BuildingList.Add (TrainingCenter);

            foreach (var item in AG.HumanList) {

                if (item.Job == Occupation.Farmer) {
                    Console.WriteLine (Farmer.CreateBuilding ("Farmer"));
                } else if (item.Job == Occupation.Monk) {
                    Console.WriteLine (Monk.CreateBuilding ("Monk"));
                } else if (item.Job == Occupation.Kinght) {
                    Console.WriteLine (Kinght.CreateBuilding ("Knight"));
                }

            }
            foreach (var item in AG.BuildingList) {

                if (item.Category == BuildingType.Farmhouse) {
                    Console.WriteLine (Farmhouse.Produce ("Farmhouse"));
                } else if (item.Category == BuildingType.Church) {
                    Console.WriteLine (Church.Produce ("Church"));
                } else if (item.Category == BuildingType.TrainingCenter) {
                    Console.WriteLine (TrainingCenter.Produce ("TrainingCenter"));
                }

            }

            Output = $"一共有{Farmer.ID}個{Farmer.Job} ,{Monk.ID}個{Monk.Job} ,{Kinght.ID}個{Kinght.Job}";
            Output += $"; 以及{Farmhouse.ID}個{Farmhouse.Category},{Church.ID}個{Church.Category} ,{TrainingCenter.ID}個{TrainingCenter.Category}";
            Console.WriteLine (Output);
        }

        public class ArmyGroup {
            public List<Human> HumanList { get; set; } = new List<Human> { };

            public List<Buliding> BuildingList { get; set; } = new List<Buliding> { };
        }

    }
}