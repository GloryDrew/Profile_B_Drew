﻿using System;
using System.Collections.Generic;

namespace Crew_Practice 
{
    class Program 
    {
        static void Main (string[] args) 
        {

            var x = new Flight { FlightNumber = "BR030" };
            var Captain = new Crew { Name = "Trebor", PS = Position.Pilot };
            var FO = new Crew { Name = "Choco", PS = Position.Pilot };
            var CA1 = new Crew { Name = "Ching", PS = Position.CA };
            var CA2 = new Crew { Name = "Sherry", PS = Position.CA };
            var CA3 = new Crew { Name = "Vera", PS = Position.CA };

            x.CrewList.Add (Captain);
            x.CrewList.Add (FO);
            x.CrewList.Add (CA1);
            x.CrewList.Add (CA2);
            x.CrewList.Add (CA3);
            string output = "";
            string final = "";
            int count =0;

            foreach (var item in x.CrewList)
            {
                if (item.PS == Position.Pilot)
                {
                    output +=   item.Name + "和";
                
                }
                if (item.PS== Position.CA)
                {
                    count++ ;
                }
            }   
            final = $"班號為{x.FlightNumber} 機師為{output.Substring(0,output.Length-1)} 組員共 {count}人";
            Console.WriteLine(final);
        }

        
        public class Flight 
        {
            public string FlightNumber { get; set; }

            public List<Crew> CrewList { get; set; } = new List<Crew> { };

        }

        public class Crew 
        {
            public string Name { get; set; }
            public Position PS { get; set; }

        }
        public enum Position 
        {
            Pilot,
            CA
        }
    }
}