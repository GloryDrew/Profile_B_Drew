using System;
using System.Linq;

namespace Employee_Practice
{
    public class Staff
    {
        public string Name {get;set;}

        public OccupationEnum Occupation {get;set;}
        
        public string Company {get;set;}

        public string Introdution => $"我是{Name}，我的職業是{Occupation}，就職於{Company}";


    }
}