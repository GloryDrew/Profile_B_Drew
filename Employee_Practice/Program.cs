﻿using System;

namespace Employee_Practice
{
    class Program
    {
        static void Main(string[] args)
        {
            var Company = new Inc {Name = "ProfileB"};

            Company.Staffs.Add(new Staff{ Name ="Trebor",Occupation = OccupationEnum.Engineer,Company=   Company.Name});
            Company.Staffs.Add(new Staff { Name="Drew",Occupation = OccupationEnum.Doctor,Company = Company.Name});
            Company.Staffs.Add(new Staff { Name="Joz",Occupation=OccupationEnum.Chef,Company = Company.Name});
            Company.Staffs.Add(new Staff { Name="Hotchi",Occupation=OccupationEnum.Teacher,Company = Company.Name});
            Company.Staffs.Add(new Staff { Name="Tee",Occupation=OccupationEnum.Doctor,Company = Company.Name});

            

            string fisrt_Print = $@"{Company.Name}公司
                                    有{Company.EmployeesCount(OccupationEnum.Engineer)}個工程師
                                    有{Company.EmployeesCount(OccupationEnum.Teacher)}個老師
                                    有{Company.EmployeesCount(OccupationEnum.Doctor)}個醫生
                                    有{Company.EmployeesCount(OccupationEnum.Chef)}個廚師";

            
            Console.WriteLine($"{fisrt_Print}");
            Company.IncIntrodution();

        }
       

    }
}
