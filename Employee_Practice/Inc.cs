using System;
using System.Collections.Generic;
using System.Linq;



namespace Employee_Practice
{
    public class Inc
    {
        public string Name{get;set;}

        public List<Staff> Staffs {get;set;} = new List<Staff>{};

        public int EmployeesCount (OccupationEnum Occupation)
        {
                return Staffs.Where (x => x.Occupation == Occupation).Count ();   

        }

        public void IncIntrodution()
        {
                foreach (var x in Staffs)
                {
                    Console.WriteLine(x.Introdution);
                    

                }

        }

    }
}